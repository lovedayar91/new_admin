<?php

use App\User;
use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('users', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name')->nullable();
            $table->string('email')->unique();
            $table->string('phone')->unique();
            $table->string('code')->nullable();
            $table->string('user_type')->nullable();
            $table->boolean('active')->nullable();
            $table->boolean('blocked')->nullable();
            $table->string('avatar')->nullable();
            $table->timestamp('email_verified_at')->nullable();
            $table->string('password');
            $table->rememberToken();
            $table->timestamps();
        });

        // Insert some stuff
        $user = new User;
        $user->name      = 'اوامر الشبكه';
        $user->email     = 'aait@aait.sa';
        $user->password  = bcrypt(123456);
        $user->phone     = '0541867992';
        $user->avatar    = 'public/user.png';
        $user->user_type = 'superAdmin';
        $user->active    = 1;
        $user->blocked   = 0;
        $user->save();
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('users');
    }
}
