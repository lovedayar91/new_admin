<?php

namespace App\Http\Controllers\api;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\User;
use Auth;
use App;
use File;
use Validator;
use Carbon\Carbon;
use App\Models\Contact;
use App\Models\Slider;
use App\Models\Rate;
use App\Models\Section;
use App\Models\City;
use App\Models\Service;
use App\Models\Service_image;
use function GuzzleHttp\json_decode;
use App\Models\Service_option;
use App\Models\Order_option;
use App\Models\Order;
use App\Models\Country;
use App\Models\Service_price;
use App\Models\Service_date;
use Illuminate\Support\Facades\DB;
use App\Models\Favourite;
use App\Models\Notification;
use App\Models\Service_address;

class ApiController extends Controller
{
    public $lang;
    public $title;

    public function __construct(Request $request)
    {
        SetLang($request->lang);
        finish_order();
        $this->lang = isset($request->lang) && $request->lang == 'en' ? 'en' : 'ar';
        $this->title = isset($request->lang) && $request->lang == 'en' ? 'title_en' : 'title_ar';

        // if provider stop his services reservation, end time or midnight active it
        finishStopReservation();
    }

    /*
    |----------------------------------------------------|
    |----------------------------------------------------|
    |                static Page Start                   |
    |----------------------------------------------------|
    |----------------------------------------------------|
    */

    #about-us
    public function about_happy(Request $request)
    {
        $data = is_null(settings('who_us_' . $this->lang)) ? '' : settings('who_us_' . $this->lang);
        /** Send Data **/
        return response()->json([
            'key'     => '1',
            'massage' => trans('api.send'),
            'data'    => fixText($data)
        ]);
    }

    #about-us
    public function about_app(Request $request)
    {
        $data = is_null(settings('about_us_' . $this->lang)) ? '' : settings('about_us_' . $this->lang);
        /** Send Data **/
        return response()->json([
            'key'     => '1',
            'massage' => trans('api.send'),
            'data'    => fixText($data)
        ]);
    }

    #about-us
    public function condition(Request $request)
    {
        $data = is_null(settings('condition_' . $this->lang)) ? '' : settings('condition_' . $this->lang);
        /** Send Data **/
        return response()->json([
            'key'     => '1',
            'massage' => trans('api.send'),
            'data'    => fixText($data)
        ]);
    }


    /*
    |----------------------------------------------------|
    |----------------------------------------------------|
    |                updateDeviceId Start                |
    |----------------------------------------------------|
    |----------------------------------------------------|
    */

    public function updateDeviceId(Request $request)
    {
        /** Validate Request **/
        $validate = Validator::make($request->all(), [
            'user_id'   => 'required|exists:users,id',
            'device_id' => 'required',
        ]);


        /** Send Error Massages **/
        if ($validate->fails()) {
            return response()->json([
                'key'     => '0',
                'massage' => $validate->errors()->first()
            ]);
        }

        /** Update User's Device_id **/
        $user = User::find($request->user_id);
        $user->device_id = $request->device_id;
        $user->save();

        /** Send Success Massage **/
        return response()->json([
            'key'     => '1',
            'massage' => 'تم التعديل'
        ]);
    }

    /*
    |----------------------------------------------------|
    |----------------------------------------------------|
    |                Contact Page Start                  |
    |----------------------------------------------------|
    |----------------------------------------------------|
    */

    public function Contact_Info(Request $request)
    {

        /** Send Data **/
        $data  = [];
        $data['phone']      = settings('phone');
        $data['whatsapp']      = social('whatsapp');
        $data['facebook']   = social('facebook');
        $data['twitter']      = social('twitter');
        $data['instagram']  = social('instagram');

        return response()->json([
            'key'     => '1',
            'massage' => trans('api.send'),
            'data'    => $data
        ]);
    }


    public function Contact_Send(Request $request)
    {
        /** Set Lang **/
        $request->lang == 'en' ? App::setLocale('en') : App::setLocale('ar');
        /** Validate Request **/
        $validate = Validator::make($request->all(), [
            'name'            => 'required|min:2|max:255',
            'email'            => 'required|min:5|max:255|email',
            'phone'            => 'required|numeric|digits_between:7,20',
            'code'          => 'required|exists:countries,code',
            'message'         => 'required',
        ]);


        /** Send Error Massages **/
        if ($validate->fails()) {
            return response()->json([
                'key'     => '0',
                'massage' => $validate->errors()->first()
            ]);
        }

        $code   = $request->code;
        $phone  = $code . convert2english($request->phone);

        /** Save Contact **/
        $contact   = new Contact;
        $contact->name    = $request->name;
        $contact->email   = $request->email;
        $contact->phone   = $phone;
        $contact->message = $request->message;
        $contact->save();


        /** Send Success Massage **/
        return response()->json([
            'key'     => '1',
            'massage' => trans('api.send')
        ]);
    }


    /*
    |----------------------------------------------------|
    |----------------------------------------------------|
    |                home Page Start                     |
    |----------------------------------------------------|
    |----------------------------------------------------|
    */

    #all section
    public function show_all_section(Request $request)
    {
        /** Send Data **/
        $data  = [];
        $title = $this->title;
        foreach (Section::activeSection() as $i => $section) {
            $data[$i]['id']            = $section->id;
            $data[$i]['title']        = $section->$title;
            $data[$i]['image']         = url('' . $section->image);
            $data[$i]['type']       = $section->type == '2' ? 'delivery' : 'static';
        }

        return response()->json([
            'key'     => '1',
            'massage' => trans('api.send'),
            'data'    => $data
        ]);
    }

    # show all sections in same type
    public function show_all_section_same_type(Request $request)
    {
        /** Validate Request **/
        $validate = Validator::make($request->all(), [
            'section_type'    => 'required|in:1,2',
            // 1 for has fixed position, 2 for delivery type
        ]);


        /** Send Error Massages **/
        if ($validate->fails()) {
            return response()->json([
                'key'     => '0',
                'massage' => $validate->errors()->first()
            ]);
        }

        $section_type = $request->section_type;

        /** Send Data **/
        $sections  = [];
        $title = $this->title;
        foreach (Section::activeSection()->where('type', $section_type) as $section) {
            $sections[] = [
                'id' => $section->id,
                'title' => $section->$title,
                'image' => url('' . $section->image),
                'type' => $section->type,
            ];
        }

        $cities = [];
        if ($section_type == 2) {
            foreach (City::get() as  $city) {
                $cities[] = [
                    'id' => $city->id,
                    'title' => $city->$title,
                    'country_id' => $city->country_id,
                ];
            }
        }

        $data = [
            'sections' => $sections,
            'cities' => $cities,
        ];

        return response()->json([
            'key'     => '1',
            'massage' => trans('api.send'),
            'data'    => $data
        ]);
    }

    /*
    |----------------------------------------------------|
    |----------------------------------------------------|
    |                service Page Start                  |
    |----------------------------------------------------|
    |----------------------------------------------------|
    */
    ############### provider pages ###############
    #show provider section's services
    public function show_provider_section_service(Request $request)
    {
        /** Validate Request **/
        $validate = Validator::make($request->all(), [
            'section_id'    => 'required|exists:sections,id',
            'user_id'       => 'required|exists:users,id',
        ]);


        /** Send Error Massages **/
        if ($validate->fails()) {
            return response()->json([
                'key'     => '0',
                'massage' => $validate->errors()->first()
            ]);
        }

        /** get service according to provider services in request section **/
        $services = Service::where('user_id', $request->user_id)->where('section_id', $request->section_id)->get();


        /** Send Data **/
        $data  = [];
        foreach ($services as $i => $service) {
            $data[$i] = showService($service, $this->lang);
        }

        return response()->json([
            'key'     => '1',
            'massage' => trans('api.send'),
            'data'    => $data
        ]);
    }

    # show provider sections
    public function providerSections(Request $request)
    {
        /** Validate Request **/
        $validate = Validator::make($request->all(), [
            'provider_id'       => 'required|exists:users,id',
        ]);


        /** Send Error Massages **/
        if ($validate->fails()) {
            return response()->json([
                'key'     => '0',
                'massage' => $validate->errors()->first()
            ]);
        }

        /** Send Data **/
        $sectionsIds = ProviderSections($request->provider_id);
        $sections = Section::activeSection()->whereIn('id', $sectionsIds);
        //dd($sections);

        $data  = [];
        $title = $this->title;
        foreach ($sections as $section) {
            $data[] = [
                'id'           => $section->id,
                'title'        => $section->$title,
                'image'         => url('' . $section->image),
            ];
        }

        return response()->json([
            'key'     => '1',
            'massage' => trans('api.send'),
            'data'    => $data
        ]);
    }

    #show by id
    public function show_service(Request $request)
    {
        /** Validate Request **/
        $validate = Validator::make($request->all(), [
            'service_id'    => 'required|exists:services,id',
            'user_id'       => 'nullable',
        ]);


        /** Send Error Massages **/
        if ($validate->fails()) {
            return response()->json([
                'key'     => '0',
                'massage' => $validate->errors()->first()
            ]);
        }

        /** get Data **/
        $service = Service::find($request->service_id);

        /** send Data **/
        if (isset($request->user_id))
            $check = Favourite::where('user_id', $request->user_id)->where('service_id', $service->id)->first();
        $fav   = isset($check) ? 1 : 0;

        $images = [];
        foreach ($service->Images as $i => $image) {
            $images[$i]['id']                = $image->id;
            $images[$i]['image']             = is_null($image->image) ? '' : url('' . $image->image);
        }

        $prices = [];
        foreach ($service->Prices as $i => $price) {
            $prices[$i]['id']                = $price->id;
            $prices[$i]['start_date']         = is_null($price->start_date) ? '' : date('Y-m-d', strtotime($price->start_date));
            $prices[$i]['end_date']         = is_null($price->end_date) ? '' : date('Y-m-d', strtotime($price->end_date));
            $prices[$i]['price']             = is_null($price->price) ? '' : $price->price;
        }

        $dates = [];
        foreach ($service->Dates as $i => $item) {
            $dates[$i]['id']            = $item->id;
            $dates[$i]['date']             = is_null($item->date) ? '' : date('Y-m-d', strtotime($item->date));
            $dates[$i]['type']             = is_null($item->type) ? 1 : (int) $item->type;
        }

        $sections_type = Section::whereIn('id', ServiceSections($service->id))->first()->type;
        $allMatchSections = Section::where('active', 1)->where('type', $sections_type)->get();
        $usedSections = ServiceSections($service->id);
        $usedSectionsIdsArray = [];
        foreach ($usedSections as $section) {
            $usedSectionsIdsArray[] = $section['id'];
        }

        /*$sections = [];
        foreach ($allMatchSections as $section){

            $sections[] = [
                'id'        => $section->id,
                'title_ar'  => (string) $section->title_ar,
                'title_en'  => (string) $section->title_en,
                'image'     => (string) url('',$section->image),
                'active'    => (string) $section->active,
                'type'      => (string) $section->type,
                'used'      => (string) (in_array($section->id,$usedSectionsIdsArray))?"1":"0",
                'created_at'=> (string) date('Y-m-d',strtotime($section->created_at))
            ];
        }*/


        $data  = [];
        $data                           = showService($service, $this->lang);
        $data['is_favourite']            = $fav;
        $data['prices']                    = $prices;
        $data['images']                    = $images;
        //$data['dates']	                = $dates;
        $data['countProviderSections']  = count(ProviderSections($service->user_id));
        //$data['all_sections']           = $sections;

        /*$Data  = Service_date::where('service_id', $service->id)->get();
	    $Dates = [];

	    if (count($Data) > 0){
		    foreach ($Data as $Dat){
			    $Dates[] = Carbon::parse($Dat['date'])->format('d-m-Y');
		    }
	    }*/

        //$data['close_dates']           = $Dates;
        // $data['options']  = $options;

        return response()->json([
            'key'     => '1',
            'massage' => trans('api.send'),
            'data'    => $data
        ]);
    }

    public function check_store_service(Request $request)
    {
        /** Validate Request **/
        $validate = Validator::make($request->all(), [
            'user_id'        => 'required|exists:users,id',
        ]);

        /** Send Error Massages **/
        if ($validate->fails()) {
            return response()->json([
                'key'     => '0',
                'massage' => $validate->errors()->first()
            ]);
        }

        //check if provider has permission to store this service
        if (!checkProviderServicesCount($request->user_id)) {
            return response()->json([
                'key'     => '0',
                'massage' => trans('api.needAdminAgree')
            ]);
        } else {
            return response()->json([
                'key'     => '1',
                'massage' => 'مسموح'
            ]);
        }
    }

    public function sendRequestToAdmin(Request $request)
    {
        /** Validate Request **/
        $validate = Validator::make($request->all(), [
            'user_id'        => 'required|exists:users,id',
        ]);

        /** Send Error Massages **/
        if ($validate->fails()) {
            return response()->json([
                'key'     => '0',
                'massage' => $validate->errors()->first()
            ]);
        }

        sendNotify($request->user_id);

        return response()->json([
            'key'     => '1',
            'massage' => trans('api.send')
        ]);
    }

    #store
    public function store_service(Request $request)
    {
        /** Validate Request **/
        $validate = Validator::make($request->all(), [
            'user_id'        => 'required|exists:users,id',
            'section_id'        => 'required|exists:sections,id',
            'city_id'        => 'nullable|exists:cities,id',
            'address_desc'   => 'nullable',
            'code'              => 'required|exists:countries,code',
            'phone'             => 'required|numeric',
            'quantity'       => 'required|numeric|max:255',
            //'payment_method' => 'required', //  canceled by owner will be visa only
            'address_ar'        => 'nullable|max:255',
            'address_en'        => 'nullable|max:255',
            'lat'            => 'nullable|max:255',
            'lng'            => 'nullable|max:255',
            'title_ar'        => 'required|max:255',
            'title_en'        => 'nullable|max:255',
            'desc_ar'        => 'required',
            //'desc_en'   	 => 'required', // canceled by owner will be ar only
            // 'options'   	 => 'required',
            'price'            => 'required',
            'prices'            => 'nullable',
            'images'            => 'required',
            'images.*'       => 'image|mimes:jpeg,png,jpg,gif,svg|max:2048',
            //'cities'   		 => 'nullable',
            //'dates'   		 => 'nullable', //  canceled by owner
            'address'        => 'nullable',
        ]);

        // owner stop it
        //check if provider has permission to store this service
        //if (!checkProviderServicesCount($request->user_id)) {
        //return response()->json([
        //'key'     => '0',
        //'massage' => trans('api.needAdminAgree')
        //]);
        //}

        /** Send Error Massages **/
        if ($validate->fails()) {
            return response()->json([
                'key'     => '0',
                'massage' => $validate->errors()->first()
            ]);
        }


        /** check required data for section type if lat lng or cities to delivery**/
        // get section type, type 1 for positional sections, type 2 for delivery sections
        /*$sectionType = Section::whereId($request->section_id)->first()->type;
        if($sectionType == 1){
            // need position -> lat+lng
            if(!$request->lat && !$request->lng){
                return response()->json([
                    'key'     => '0',
                    'massage' => trans('api.needPosition')
                ]);
            }

        }elseif($sectionType == 2){
            // need cities for delivery
            //            if(!$request->cities){
            //                return response()->json([
            //                    'key'     => '0',
            //                    'massage' => trans('api.needCities')
            //                ]);
            //            }else{
            if($request->cities){
                ### check data ###
                // get request ids to array
                $request_cities_ids = explode(",",$request->cities);
                //$request_cities_ids = json_decode($request_cities_ids);
                // get exist section ids to array
                $exist_cities_ids = City::pluck('id')->toArray();
                // check given ids if exist in table sections=categories
                if(count($request_cities_ids) > 0){
                    foreach($request_cities_ids as $id){
                        if(in_array($id,$exist_cities_ids) && preg_match("/^[0-9]+$/", $id)){
                            continue;
                        }else{
                            return response()->json([
                                'key'     => '0',
                                'massage' => trans('api.wrongCity')
                            ]);
                        }
                    }
                    // must one id at least
                }else{
                    return response()->json([
                        'key'     => '0',
                        'massage' => trans('api.wrongCity')
                    ]);
                }

            }
        }*/

        //check all prices data
        $all_dates = [];
        if (!empty($request->prices)) {
            foreach (json_decode($request->prices) as $price) {
                if ($price->price < 0) {
                    return response()->json([
                        'key'     => '0',
                        'massage' => trans('api.priceLess')
                    ]);
                }

                if (Carbon::parse($price->start_date)->lessThan(Carbon::parse($price->end_date))) {
                    $dates = createDateRangeArray($price->start_date, $price->end_date);
                    foreach ($dates as $date) {
                        if (Carbon::parse($date)->isPast()) {
                            return response()->json([
                                'key'     => '0',
                                'massage' => trans('api.oldDate')
                            ]);
                        }

                        if (in_array($date, $all_dates)) {
                            return response()->json([
                                'key'     => '0',
                                'massage' => trans('api.sameDate')
                            ]);
                        }

                        array_push($all_dates, $date);
                    }
                } else {
                    return response()->json([
                        'key'     => '0',
                        'massage' => trans('api.wrongDate')
                    ]);
                }
            }
        }

        //check all prices data
        /*$all_new_dates = [];
        if($request->dates && $request->dates != "[{}]"){
            foreach (json_decode($request->dates) as $item) {

                if(Carbon::parse($item->start_date)->lessThanOrEqualTo(Carbon::parse($item->end_date))){
                    $dates = createDateRangeArray($item->start_date, $item->end_date);
                    foreach ($dates as $date){
                        if(Carbon::parse($date)->isPast() && !Carbon::parse($date)->isToday()){
                            return response()->json([
                                'key'     => '0',
                                'massage' => trans('api.oldDate')
                            ]);
                        }

                        array_push($all_new_dates,$date);
                    }
                }else{
                    return response()->json([
                        'key'     => '0',
                        'massage' => trans('api.wrongDate')
                    ]);
                }

            }
        }*/

        // get country code for phone use
        $code = $request->code;

        /** store Data **/
        $service = new Service;
        $service->title_ar          = $request->title_ar;
        $service->title_en          = $request->title_en != null ? $request->title_en : $request->title_ar;
        $service->desc_ar          = $request->desc_ar;
        $service->desc_en          = $request->desc_ar; // canceled by owner will be ar only
        $service->address          = $request->address_desc;
        $service->address_ar      = $request->address_ar;
        $service->address_en      = $request->address_en;
        $service->lat              = $request->lat;
        $service->lng              = $request->lng;
        $service->price          = $request->price;
        $service->main_phone      = $request->phone;
        $service->phone          = $code . $request->phone;
        $service->quantity          = $request->quantity;
        $service->payment_method = 1; //  canceled by owner will be visa only
        $service->user_id          = $request->user_id;
        $service->country_id      = find_country_id($request->code);
        $service->section_id      = $request->section_id;
        $service->city_id          = $request->city_id;
        $service->close          = User::whereId($request->user_id)->first()->stop_service;
        // this subcategory has more than one category -> save as string "1,2"
        //$service->cities         = $request->cities ? $request->cities : "" ;

        // add adrress for search by admin -> will be array in different table
        //$request->address ? $service->address 		 = $request->address:"";
        $service->save();


        //store addresses for search
        if (!empty($request->address)) {
            foreach (json_decode($request->address) as $address) {
                $new = new Service_address;
                $new->service_id    = $service->id;
                $new->address         = $address->address;
                $new->city_id         = $address->city_id;
                $new->save();
            }
        }

        //store images
        foreach ($request->file('images') as $photo) {
            $service_image = new Service_image;
            $service_image->service_id  = $service->id;
            $service_image->image       = uploadImage($photo, 'public/images/services');
            $service_image->save();
        }

        //store prices
        if (!empty($request->prices)) {
            foreach (json_decode($request->prices) as $price) {
                $service_price = new Service_price;
                $service_price->service_id  = $service->id;
                $service_price->price         = $price->price;
                $service_price->start_date     = $price->start_date;
                $service_price->end_date     = $price->end_date;
                $service_price->save();
            }
        }

        /*if(!is_null($request->dates) && $request->dates != "[{}]"){
            foreach (json_decode($request->dates) as $item) {
                if(Carbon::parse($item->start_date)->lessThan(Carbon::parse($item->end_date))){
                    $dates = createDateRangeArray($item->start_date, $item->end_date);
                    foreach ($dates as $date){
                        $check = Service_date::whereDate('date' , $date)->where('type',$item->type)->where('service_id',$service->id)->first();
                        if(isset($check)) continue;
                        $service_date = new Service_date;
                        $service_date->service_id   = $service->id;
                        $service_date->type 	    = $item->type;
                        $service_date->date 		= $date;
                        $service_date->save();
                    }
                }
            }
        }*/

        // foreach (json_decode($request->options) as $option) {
        // 	$service_option = new Service_option;
        // 	$service_option->service_id = $service->id;
        // 	$service_option->title 		= $option->title;
        // 	$service_option->save();
        // }

        /** send Data **/
        // $title = $this->title;
        // $options = [];
        // foreach ($service->Options as $i => $option) {
        // 	$options[$i]['id']    			= $option->id;
        // 	$options[$i]['title'] 			= is_null($option->$title) ? '' : $option->$title;
        // }

        $images = [];
        foreach ($service->Images as $i => $image) {
            $images[$i]['id']                = $image->id;
            $images[$i]['image']             = is_null($image->image) ? '' : url('' . $image->image);
        }

        $prices = [];
        foreach ($service->Prices as $i => $price) {
            $prices[$i]['id']                = $price->id;
            $prices[$i]['start_date']         = is_null($price->start_date) ? '' : date('Y-m-d', strtotime($price->start_date));
            $prices[$i]['end_date']         = is_null($price->end_date) ? '' : date('Y-m-d', strtotime($price->end_date));
            $prices[$i]['price']             = is_null($price->price) ? '' : $price->price;
        }

        $data  = [];
        $data  = showService($service, $this->lang);
        $data['images']      = $images;
        $data['prices']      = $prices;
        // $data['options']  = $options;

        return response()->json([
            'key'     => '1',
            'massage' => trans('api.save'),
            'data'    => $data
        ]);
    }

    #update
    public function update_service(Request $request)
    {
        //dd($request->all());
        /** Validate Request **/
        $validate = Validator::make($request->all(), [
            'service_id'        => 'required|exists:services,id',
            'section_id'        => 'required|exists:sections,id',
            'city_id'        => 'nullable|exists:cities,id',
            'address_desc'   => 'nullable',
            'code'              => 'required|exists:countries,code',
            'phone'             => 'required|numeric',
            'quantity'       => 'required|numeric|max:255',
            //'payment_method' => 'required', //  canceled by owner will be visa only
            'address_ar'        => 'nullable|max:255',
            'address_en'        => 'nullable|max:255',
            'lat'            => 'nullable|max:255',
            'lng'            => 'nullable|max:255',
            'title_ar'        => 'required|max:255',
            'title_en'        => 'nullable|max:255',
            'desc_ar'        => 'required',
            //'desc_en'   	 => 'required', // canceled by owner will be ar only
            // 'options'   	 => 'required',
            'price'            => 'required',
            'prices'            => 'nullable',
            'images'            => 'nullable',
            'images.*'       => 'image|mimes:jpeg,png,jpg,gif,svg|max:2048',
            //'cities'   		 => 'nullable',
            //'dates'   		 => 'nullable', //  canceled by owner
            'address'        => 'nullable',
        ]);


        /** Send Error Massages **/
        if ($validate->fails()) {
            return response()->json([
                'key'     => '0',
                'massage' => $validate->errors()->first()
            ]);
        }

        // check sections data if request
        //        if(!is_null($request->section_id)){
        //            /** handle section ids -> one service has many parents may categories **/
        //            // get request ids to array
        //            $request_sections_ids = explode(",",$request->section_id);
        //            // get exist section ids to array
        //            $exist_sections_ids = Section::pluck('id')->toArray();
        //            // check given ids if exist in table sections=categories
        //            if(count($request_sections_ids) > 0){
        //                foreach($request_sections_ids as $id){
        //                    if(in_array($id,$exist_sections_ids) && preg_match("/^[0-9]+$/", $id)){
        //                        continue;
        //                    }else{
        //                        return response()->json([
        //                            'key'     => '0',
        //                            'massage' => trans('api.wrongSection')
        //                        ]);
        //                    }
        //                }
        //                // must one id at least
        //            }else{
        //                return response()->json([
        //                    'key'     => '0',
        //                    'massage' => trans('api.wrongSection')
        //                ]);
        //            }
        //        }
        //        // check if sections has same type
        //        if(count($request_sections_ids) > 0){
        //            if(CheckSectionsType($request_sections_ids)){
        //                return response()->json([
        //                    'key'     => '0',
        //                    'massage' => trans('api.unMatchesSectionsTypes')
        //                ]);
        //            };
        //        }
        //        // check cities data if request
        //        if(!is_null($request->cities)){
        //            // get request ids to array
        //            $request_cities_ids = explode(",",$request->cities);
        //            // get exist section ids to array
        //            $exist_cities_ids = City::pluck('id')->toArray();
        //            // check given ids if exist in table sections=categories
        //            if(count($request_cities_ids) > 0){
        //                foreach($request_cities_ids as $id){
        //                    if(in_array($id,$exist_cities_ids) && preg_match("/^[0-9]+$/", $id)){
        //                        continue;
        //                    }else{
        //                        return response()->json([
        //                            'key'     => '0',
        //                            'massage' => trans('api.wrongCity')
        //                        ]);
        //                    }
        //                }
        //            }
        //        }



        //check all prices data
        $all_dates = [];
        if (!empty($request->prices)) {
            foreach (json_decode($request->prices) as $price) {
                if ($price->price < 0) {
                    return response()->json([
                        'key'     => '0',
                        'massage' => trans('api.priceLess')
                    ]);
                }

                if (Carbon::parse($price->start_date)->lessThan(Carbon::parse($price->end_date))) {
                    $dates = createDateRangeArray($price->start_date, $price->end_date);
                    foreach ($dates as $date) {
                        if (Carbon::parse($date)->isPast()) {
                            return response()->json([
                                'key'     => '0',
                                'massage' => trans('api.oldDate')
                            ]);
                        }

                        if (in_array($date, $all_dates)) {
                            return response()->json([
                                'key'     => '0',
                                'massage' => trans('api.sameDate')
                            ]);
                        }

                        array_push($all_dates, $date);
                    }
                } else {
                    return response()->json([
                        'key'     => '0',
                        'massage' => trans('api.wrongDate')
                    ]);
                }
            }
        }

        /*$all_new_dates = [];
        if(!is_null($request->dates) && $request->dates != "[{}]"){
            foreach (json_decode($request->dates) as $item) {

                if(Carbon::parse($item->start_date)->lessThanOrEqualTo(Carbon::parse($item->end_date))){
                    $dates = createDateRangeArray($item->start_date, $item->end_date);
                    foreach ($dates as $date){
                        if(Carbon::parse($date)->isPast() && !Carbon::parse($date)->isToday()){
                            return response()->json([
                                'key'     => '0',
                                'massage' => trans('api.oldDate')
                            ]);
                        }

                        array_push($all_new_dates,$date);
                    }
                }else{
                    return response()->json([
                        'key'     => '0',
                        'massage' => trans('api.wrongDate')
                    ]);
                }

            }
        }*/

        // get country code for phone use
        $code = $request->code;

        /** update Data **/
        $service = Service::find($request->service_id);
        $service->title_ar          = $request->title_ar;
        $service->title_en          = $request->title_en != null ? $request->title_en : $request->title_ar;
        $service->desc_ar          = $request->desc_ar;
        $service->desc_en          = $request->desc_ar; // canceled by owner will be ar only
        $service->address          = $request->address_desc;
        $service->address_ar      = $request->address_ar;
        $service->address_en      = $request->address_en;
        $service->lat              = $request->lat;
        $service->lng              = $request->lng;
        $service->price          = $request->price;
        $service->main_phone      = $request->phone;
        $service->phone          = $code . $request->phone;
        $service->quantity          = $request->quantity;
        $service->payment_method = 1; //  canceled by owner will be visa only
        $service->country_id      = find_country_id($request->code);

        // save sections ids -> more than one section
        if (!is_null($request->section_id))
            $service->section_id      = $request->section_id;

        // save sections ids -> more than one section
        if (!is_null($request->city_id))
            $service->city_id      = $request->city_id;

        //$request->address ? $service->address 		 = $request->address:"";

        $service->save();

        //delete old prices & store new prices
        Service_address::where('service_id', $request->service_id)->delete();
        //store addresses for search
        if (!empty($request->address)) {
            foreach (json_decode($request->address) as $address) {
                $new = new Service_address;
                $new->service_id    = $service->id;
                $new->address         = $address->address;
                $new->city_id         = $address->city_id;
                $new->save();
            }
        }

        // delete old images
        $delete_image_id = empty($request->delete_image_id) ? [] : explode(',', $request->delete_image_id);
        if (!empty($delete_image_id))
            Service_image::whereIn('id', $delete_image_id)->where('service_id', $request->service_id)->delete();
        if (!empty($request->images)) {
            //store images
            foreach ($request->file('images') as $photo) {
                $service_image = new Service_image;
                $service_image->service_id  = $service->id;
                $service_image->image       = uploadImage($photo, 'public/images/services');
                $service_image->save();
            }
        }

        //delete old prices & store new prices
        Service_price::where('service_id', $request->service_id)->delete();
        //store prices
        if (!empty($request->prices)) {
            foreach (json_decode($request->prices) as $price) {
                $service_price = new Service_price;
                $service_price->service_id  = $service->id;
                $service_price->price         = $price->price;
                $service_price->start_date     = $price->start_date;
                $service_price->end_date     = $price->end_date;
                $service_price->save();
            }
        }

        /*//store addresses for search
        if(!is_null($request->address)){

            foreach($service->Addresses as $address){
                $address->delete();
            }

            $addresses = json_decode($request->address);
            $newAdd = [];
            foreach ($addresses as $key){
                foreach ($key as $key1=>$val1){
                    $newAdd[]=$val1;
                }
            }
            foreach ($newAdd as $val) {
                $new = new Service_address;
                $new->service_id    = $service->id;
                $new->address 		= $val;
                $new->save();

            }
        }

        // delete old images
        $delete_image_id = empty($request->delete_image_id) ? [] : explode(',', $request->delete_image_id);
        if (!empty($delete_image_id))
            Service_image::whereIn('id', $delete_image_id)->where('service_id', $request->service_id)->delete();
        //store new images
        if ($request->has('images')) {
            foreach ($request->file('images') as $photo) {
                $service_image = new Service_image;
                $service_image->service_id = $service->id;
                $service_image->image = uploadImage($photo, 'public/images/services');
                $service_image->save();
            }
        }


        //delete old prices & store new prices
        Service_price::where('service_id', $request->service_id)->delete();
        if(!is_null($request->prices)){
            foreach (json_decode($request->prices) as $price) {
                $service_price = new Service_price;
                $service_price->service_id  = $service->id;
                $service_price->price 		= $price->price;
                $service_price->start_date 	= $price->start_date;
                $service_price->end_date 	= $price->end_date;
                $service_price->save();
            }
        }

        //Service_date::where('service_id', $request->service_id)->delete();
        if(!is_null($request->dates) && $request->dates != "[{}]"){
            foreach (json_decode($request->dates) as $item) {
                if(Carbon::parse($item->start_date)->lessThan(Carbon::parse($item->end_date))){
                    $dates = createDateRangeArray($item->start_date, $item->end_date);
                    foreach ($dates as $date){
                        $check = Service_date::whereDate('date' , $date)->where('type',$item->type)->where('service_id',$service->id)->first();
                        if(isset($check)) continue;
                        $service_date = new Service_date;
                        $service_date->service_id   = $service->id;
                        $service_date->type 	    = $item->type;
                        $service_date->date 		= $date;
                        $service_date->save();
                    }
                }
            }
        }*/

        $images = [];
        foreach ($service->Images as $i => $image) {
            $images[$i]['id']                = $image->id;
            $images[$i]['image']             = is_null($image->image) ? '' : url('' . $image->image);
        }

        $prices = [];
        foreach ($service->Prices as $i => $price) {
            $prices[$i]['id']                = $price->id;
            $prices[$i]['start_date']         = is_null($price->start_date) ? '' : date('Y-m-d', strtotime($price->start_date));
            $prices[$i]['end_date']         = is_null($price->end_date) ? '' : date('Y-m-d', strtotime($price->end_date));
            $prices[$i]['price']             = is_null($price->price) ? '' : $price->price;
        }

        $data  = [];
        $data  = showService($service, $this->lang);
        $data['images']      = $images;
        $data['prices']      = $prices;
        // $data['options']  = $options;

        return response()->json([
            'key'     => '1',
            'massage' => trans('api.save'),
            'data'    => $data
        ]);
    }

    public function delete_address(Request $request)
    {
        /** Validate Request **/
        $validate = Validator::make($request->all(), [
            'address_id'        => 'required|exists:service_addresses,id',
        ]);

        /** Send Error Massages **/
        if ($validate->fails()) {
            return response()->json([
                'key'     => '0',
                'massage' => $validate->errors()->first()
            ]);
        }


        Service_address::whereId($request->address_id)->first()->delete();

        return response()->json([
            'key'     => '1',
            'massage' => trans('api.delete'),
        ]);
    }

    public function delete_date(Request $request)
    {
        /** Validate Request **/
        $validate = Validator::make($request->all(), [
            'date_id'        => 'required|exists:service_dates,id',
        ]);


        /** Send Error Massages **/
        if ($validate->fails()) {
            return response()->json([
                'key'     => '0',
                'massage' => $validate->errors()->first()
            ]);
        }

        Service_date::whereId($request->date_id)->delete();

        return response()->json([
            'key'     => '1',
            'massage' => trans('api.delete'),
        ]);
    }

    #share
    public function share_service(Request $request)
    {
        /** Validate Request **/
        $validate = Validator::make($request->all(), [
            'service_id'        => 'required|exists:services,id',
        ]);


        /** Send Error Massages **/
        if ($validate->fails()) {
            return response()->json([
                'key'     => '0',
                'massage' => $validate->errors()->first()
            ]);
        }

        return response()->json([
            'key'     => '1',
            'massage' => trans('api.save'),
            'data'    => url('show-service', $request->service_id)
        ]);
    }

    #delete
    public function delete_service(Request $request)
    {
        /** Validate Request **/
        $validate = Validator::make($request->all(), [
            'service_id'        => 'required|exists:services,id',
        ]);


        /** Send Error Massages **/
        if ($validate->fails()) {
            return response()->json([
                'key'     => '0',
                'massage' => $validate->errors()->first()
            ]);
        }

        #Send FCM To orders's Client
        $all_orders = Order::where('service_id', $request->service_id)->where('user_id', '!=', null)->get();
        foreach ($all_orders as $order) {
            $order->provider_seen   = 0;
            $order->user_seen       = 0;
            $order->save();
            $user = User::find($order->user_id);
            if (isset($user) && !is_null($user->device_id)) {
                $device_id          = $user->device_id;
                $device_type        = $user->device_type;
                $lang = $user->lang;
                $data = [
                    'title_ar'    => 'الطلبات',
                    'title_en'    => 'orders',
                    'active_ar'   => 'تم حذف الخدمة وتم الغاء طلبك برقم : ' . $order->id,
                    'active_en'   => 'service is deleted and your order no: ' . $order->id . ' is canceled',
                ];
                $all_data   = [];
                $all_data['title']      = $data['title_' . $lang];
                $all_data['msg']        = $data['active_' . $lang];
                $all_data['status']     = 3; //order canseled

                Send_FCM_Badge($device_id, $all_data, $device_type);
            }
        }

        //delete service
        //Service::find($request->service_id)->delete();

        $service = Service::find($request->service_id);
        foreach ($service->Images as $image) {
            if (File::exists($image->image)) {
                File::delete($image->image);
            }
        }
        $service->delete();

        return response()->json([
            'key'     => '1',
            'massage' => trans('api.delete')
        ]);
    }

    # stop reserve provider services for today
    public function ProviderStopReserve(Request $request)
    {
        //return(Carbon::now()->startOfDay()->addDay());
        /** Validate Request **/
        $validate = Validator::make($request->all(), [
            'provider_id'        => 'required|exists:users,id',
            'end_time'           => 'nullable',
        ]);


        /** Send Error Massages **/
        if ($validate->fails()) {
            return response()->json([
                'key'     => '0',
                'massage' => $validate->errors()->first()
            ]);
        }

        $provider = User::whereId($request->provider_id)->first();
        if ($provider->stop_reserving == 0) {
            $provider->stop_reserving = 1;
            $provider->stop_start_time = Carbon::now();
            $provider->stop_end_time = $request->end_time ? $request->end_time : Carbon::now()->startOfDay()->addDay();
            $provider->save();
        } else {
            $provider->stop_reserving = 0;
            $provider->save();
        }


        return response()->json([
            'key'     => '1',
            'massage' => trans('api.save')
        ]);
    }


    ############### user pages ###############


    # show section data by providers with cheapest service data
    public function show_service_by_section(Request $request)
    {
        /** Validate Request **/
        $validate = Validator::make($request->all(), [
            'section_id'    => 'nullable|exists:sections,id',
        ]);

        /** Send Error Massages **/
        if ($validate->fails()) {
            return response()->json([
                'key'     => '0',
                'massage' => $validate->errors()->first()
            ]);
        }

        /** Get Data **/
        $sectionServicesIds = ArrayServicesSections($request->section_id);
        $providersIds = [];
        foreach ($sectionServicesIds as $service_id) {
            $provider_id = Service::whereId($service_id)->first()->user_id;
            if (!in_array($provider_id, $providersIds)) {
                $providersIds[] = $provider_id;
            }
        }

        /** Send Data **/
        $data  = [];
        $section = Section::whereId($request->section_id)->first();
        $title = $this->title;
        foreach ($providersIds as $provider_id) {
            $provider = User::whereId($provider_id)->first();
            $service = Service::where('user_id', $provider->id)->where('section_id', $section->id)->orderBy('price', 'desc')->first();
            $data[] = [
                'section_id'            => $section->id,
                'section_name'          => (string) $section->$title,
                'provider_id'           => $provider->id,
                'provider_name'         => (string) $provider->name,
                'provider_image'        => is_null($provider->avatar) ? url('images/users/default.png') : url('public/images/users/' . $provider->avatar),
                'service_id'            => $service->id,
                'service_price'         => (string) $service->price,
                'service_countRate'     => (string) $service->countRate,
                'service_finalRate'     => (string) $service->finalRate,
                'service_desc'          => (string) $this->lang == 'en' ? $service->desc_en : $service->desc_ar,
                'service_quantity'      => (string) $service->quantity,
            ];
        }


        return response()->json([
            'key'     => '1',
            'massage' => trans('api.send'),
            'data'    => $data
        ]);
    }

    ////////////// filter and search data //////////////
    //    public function FilterAll(Request $request){
    //        /** Validate Request **/
    //        $validate = Validator::make($request->all(), [
    //            'section_id'    => 'required|exists:sections,id',
    //            'provider_id'   => 'nullable|exists:users,id',
    //            'user_id'       => 'nullable|exists:users,id',
    //            'lat'   		=> 'required_with:lng',
    //            'lng'   		=> 'required_with:lat',
    //            'start_date'   	=> 'required_with:end_date',
    //            'end_date'   	=> 'required_with:start_date',
    //            'order_by'   	=> 'nullable|in:0,1,2,3',
    //        ]);
    //
    //        /** Send Error Massages **/
    //        if ($validate->fails()) {
    //            return response()->json([
    //                'key'     => '0',
    //                'massage' => $validate->errors()->first()
    //            ]);
    //        }
    //
    //        /** Get provider ids which have services in this section **/
    //        $sectionServicesIds = ArrayServicesSections($request->section_id);
    //        $providersIds = [];
    //        foreach($sectionServicesIds as $service_id){
    //            $provider_id = Service::whereId($service_id)->first()->user_id;
    //            if(!in_array($provider_id,$providersIds)){
    //                $providersIds[]= $provider_id;
    //            }
    //        }
    //
    //        $data  = [];
    //        $section = Section::whereId($request->section_id)->first();
    //        $title = $this->title;
    //
    //        /** Get cheapest service id for each provider **/
    //        $servicesIds = [];
    //        foreach($providersIds as $provider_id){
    //            $provider = User::whereId($provider_id)->first();
    //            $service = Service::where('user_id',$provider->id)->whereIn('id',$sectionServicesIds)->orderBy('price','asc')->first();
    //            if($service){
    //                $servicesIds[]=$service->id;
    //            }
    //        }
    //
    //
    //        /** if request provider id change service ids to provider services only in request section **/
    //        if($request->provider_id){
    //            $servicesIds = Service::where('user_id',$request->provider_id)->whereIn('id',$sectionServicesIds)->pluck('id')->toArray();
    //        }
    //
    //
    //        /** search inside this services if search request **/
    //        $results 		= Service::query();
    //        $results->whereIn('id', $servicesIds);
    //
    //        if(!is_null($request->address)){
    //            $servicesIdsFromAddress = Service_address::Where('address', 'like', '%' . $request->address . '%')
    //                                        ->distinct('service_id')->pluck('service_id');
    //
    //            $results->whereIn('id', $servicesIdsFromAddress)
    //                    ->orWhere('address_ar', 'like', '%' . $request->address . '%')->whereIn('id', $servicesIds);
    //        }
    //
    //
    //
    //        if (!is_null($request->lat) && !is_null($request->lng)) {
    //            $lat  = $request->lat;
    //            $lng  = $request->lng;
    //            $results->having('distance', '<=', '500')
    //                ->select(
    //                    DB::raw("*,
    //                (3959 * ACOS(COS(RADIANS($lat))
    //                * COS(RADIANS(lat))
    //                * COS(RADIANS($lng) - RADIANS(lng))
    //                + SIN(RADIANS($lat))
    //                * SIN(RADIANS(lat)))) AS distance")
    //                );
    //        }
    //
    //
    //        /** filter this services if request filter **/
    //        // 0=>mostRent , 1=>hightPrice , 2=>lowPrice, 3=> finalRate
    //        if ($request->order_by == '0')
    //            $results->orderBy('rent_count', 'desc');
    //        elseif ($request->order_by == '1')
    //            $results->orderBy('price', 'desc');
    //        elseif ($request->order_by == '2')
    //            $results->orderBy('price', 'asc');
    //        elseif ($request->order_by == '3')
    //            $results->orderBy('finalRate', 'desc');
    //        else
    //            $results->orderBy('id', 'desc');
    //
    //        $all_services = $results->get();
    //
    //        $title = $this->title;
    //        /** return data **/
    //        // check which stock available in request date
    //        if($request->start_date && $request->end_date){
    //            foreach ($all_services as $service) {
    //                if(checkServiceDateQuantity($service->id,$request->start_date,$request->end_date)){
    //                    $provider = User::whereId($service->user_id)->first();
    //                    $data[]=[
    //                        'section_id'            => $section->id,
    //                        'section_name'          => (string) $section->$title,
    //                        'provider_id'           => $provider->id,
    //                        'provider_name'         => (string) $provider->name,
    //                        'provider_rate'         => number_format($provider->finalRate,1),
    //                        'provider_image'        => is_null($provider->avatar) ? url('images/users/default.png') : url('public/images/users/' . $provider->avatar),
    //                        'service_id'            => $service->id,
    //                        'service_name'          => $service->$title,
    //                        'service_image'         => isset($service->Images)? url('',$service->Images->first()->image):"",
    //                        'service_price'         => (string) $service->price,
    //                        'service_countRate'     => (string) $service->countRate,
    //                        'service_finalRate'     => number_format($service->finalRate,1),
    //                        'service_desc'          => (string) $this->lang == 'en' ? $service->desc_en:$service->desc_ar,
    //                        'service_quantity'      => (string) ($service->quantity - getQuantity($service->id,$request->start_date)),
    //                        'service_lat'           => (string) $service->lat,
    //                        'service_lng'           => (string) $service->lng,
    //                        'is_fav'                => $request->user_id ? IsFav($request->user_id,$service->id):"",
    //
    //                        //'address'=>$service->address,
    //                        //'address_ar'=>$service->address_ar,
    //                        //'sections'=>$service->section_id,
    //                    ];
    //                }
    //            }
    //        // all data if not request date
    //        }else{
    //            foreach($all_services as $service){
    //                $provider = User::whereId($service->user_id)->first();
    //                $data[]=[
    //                    'section_id'            => $section->id,
    //                    'section_name'          => (string) $section->$title,
    //                    'provider_id'           => $provider->id,
    //                    'provider_name'         => (string) $provider->name,
    //                    'provider_rate'         => number_format($provider->finalRate,1),
    //                    'provider_image'        => is_null($provider->avatar) ? url('images/users/default.png') : url('public/images/users/' . $provider->avatar),
    //                    'service_id'            => $service->id,
    //                    'service_name'          => $service->$title,
    //                    'service_image'         => isset($service->Images)? url('',$service->Images->first()->image):"",
    //                    'service_price'         => (string) $service->price,
    //                    'service_countRate'     => (string) $service->countRate,
    //                    'service_finalRate'     => number_format($service->finalRate,1),
    //                    'service_desc'          => (string) $this->lang == 'en' ? $service->desc_en:$service->desc_ar,
    //                    'service_quantity'      => (string) $service->quantity,
    //                    'service_lat'           => (string) $service->lat,
    //                    'service_lng'           => (string) $service->lng,
    //                    'is_fav'                => $request->user_id ? IsFav($request->user_id,$service->id):"",
    //
    //
    //                    //'address'=>$service->address,
    //                    //'address_ar'=>$service->address_ar,
    //                    //'sections'=>$service->section_id,
    //                ];
    //            }
    //        }
    //
    //        return response()->json([
    //            'key'     => '1',
    //            'massage' => trans('api.send'),
    //            'data'    => $data
    //        ]);
    //    }

    #fill data to use in auto-compelete
    public function Fill_data(Request $request)
    {

        $validate = Validator::make($request->all(), [
            'section_id'    => 'required|exists:sections,id',
            'city_id'       => 'nullable',
        ]);

        /** Send Error Massages **/
        if ($validate->fails()) {
            return response()->json([
                'key'     => '0',
                'massage' => $validate->errors()->first()
            ]);
        }

        $section_id = $request->section_id;
        $city_id    = $request->city_id;

        $results  = Service_address::query();
        //if(is_null($city_id)) $results->distinct('city_id');
        $results->whereHas('Service', function ($q) use ($section_id) {
            return $q->where('section_id', $section_id);
        });
        if (!is_null($city_id)) $results->where('city_id', $city_id);
        $all_data = $results->get();

        $data = [];
        $title = $this->title;
        foreach ($all_data as $item) {
            $addresses = Service_address::where('city_id', $item->city_id)->where('service_id', $item->service_id)->get();
            if (is_in_array($data, 'id', $item->city_id)) continue;
            $address = [];
            foreach ($addresses as $address_details) {
                $address[] = [
                    'address'        => (string) $address_details->address,
                ];
            }
            $data[] = [
                'id'             => (int) $item->city_id,
                'title'          => is_null($item->City) ? '' : (string) $item->City->$title,
                'address'        => $address,
            ];
        }


        return response()->json(['key' => 1, 'massage' => 'sent', 'data' => $data]);
    }

    #filter all
    public function FilterAll(Request $request)
    {

        $validator = Validator::make($request->all(), [
            'section_id'    => 'required|exists:sections,id',
            'address'       => 'nullable',
            'city_id'       => 'nullable',
            'start_date'    => 'nullable',
            'end_date'      => 'nullable',
            'order_by'      => 'nullable|in:low-price,high-price,high-rate',
            'provider_id'   => 'nullable|exists:users,id',
            'user_id'       => 'nullable|exists:users,id',
            'lang'          => 'required|in:ar,en'
        ], [
            'order_by.in' => 'order_by Values [low-price - high-price - high-rate]'
        ]);

        if ($validator->passes()) {

            $title      = $this->title;
            $section    = Section::whereId($request->section_id)->first();
            $results    = Service::query();
            $results->where('section_id', $request->section_id);
            $results->whereHas('User', function ($q) {
                $q->where('checked', '1')->where('activation', '1');
            });

            if (!empty($request['address'])) {
                $results->whereHas('Addresses', function ($q) use ($request) {
                    $q->where('address', 'like', '%' . $request['address'] . '%');
                });
            }

            if (!empty($request['city_id'])) {
                $results->whereHas('Addresses', function ($q) use ($request) {
                    $q->where('city_id', $request['city_id']);
                });
            }

            if (!empty($request['start_date'])) {
                $results->whereHas('Order', function ($q) use ($request) {
                    $q->whereDate('start_date', '!=', Carbon::parse($request['start_date']));
                });
            }

            if (!empty($request['end_date'])) {
                $results->whereHas('Order', function ($q) use ($request) {
                    $q->whereDate('end_date', '!=', Carbon::parse($request['end_date']));
                });
            }

            if (!empty($request['provider_id'])) {
                $results->where('user_id', $request->provider_id);
            }

            if (!empty($request['order_by'])) {
                if ($request['order_by'] == 'low-price') {
                    $results = $results->orderBy('price', 'asc');
                } elseif ($request['order_by'] == 'high-price') {
                    $results = $results->orderBy('price', 'desc');
                } else {
                    $results = $results->orderBy('finalRate', 'desc');
                }
            }

            $services_data = $results->get();
            $services = [];
            $user_ids = [];
            foreach ($services_data as $service) {
                if (!in_array($service->user_id, $user_ids) || !empty($request->provider_id)) {
                    array_push($user_ids, $service->user_id);
                    $services[] = [
                        'id'                    => $service->id,
                        'close'                 => $service->close == 1 ? true : false,
                        'image'                 => isset($service->Images) ? url('' . $service->Images->first()->image) : "",
                        'section'               => $service->Section['title_' . $request['lang']],
                        'price'                 => $service->price,
                        'name'                  => isset($service->User['first_name']) ? $service->User['first_name'] : '',
                        'rate'                  => number_format($service->User->finalRate, 1),
                        'desc'                  => (string) $this->lang == 'en' ? $service->desc_en : $service->desc_ar,
                        'quantity'              => (string) $service->quantity,
                        'user_id'               => (string) $service->User->id,
                        'is_fav'                => isset($request['user_id']) ? IsFav($request['user_id'], $service->id) : "",

                        'section_name'          => (string) $section->$title,
                        'provider_id'           => isset($service->User['id']) ? $service->User['id'] : 0,
                        'provider_name'         => isset($service->User['first_name']) ? $service->User['first_name'] : '',
                        'provider_rate'         => number_format($service->User['finalRate'], 1),
                        'provider_image'        => is_null($service->User['avatar']) ? url('images/users/default.png') : url('public/images/users/' . $service->User['avatar']),
                        'service_id'            => $service->id,
                        'service_name'          => $service->$title,
                        'service_image'         => isset($service->Images) ? url('' . $service->Images->first()->image) : "",
                        'service_price'         => (string) $service->price,
                        'service_countRate'     => (string) $service->countRate,
                        'service_finalRate'     => number_format($service->finalRate, 1),
                        'service_desc'          => (string) $this->lang == 'en' ? $service->desc_en : $service->desc_ar,
                        'service_quantity'      => (string) ($service->quantity - getQuantity($service->id, $request->start_date)),
                        'service_lat'           => (string) $service->lat,
                        'service_lng'           => (string) $service->lng,
                        'is_fav'                => $request->user_id ? IsFav($request->user_id, $service->id) : "",
                    ];
                }
            }

            return response()->json(['key' => 1, 'value' => '1', 'search' => $services, 'services' => $services]);
        } else {
            foreach ((array) $validator->errors() as $key => $value) {
                foreach ($value as $msg) {
                    return response()->json(['key' => 0, 'value' => '0', 'msg' => $msg[0]]);
                }
            }
        }
    }

    #show by filter
    public function show_service_by_filter(Request $request)
    {
        /** Validate Request **/
        $validate = Validator::make($request->all(), [
            'user_id'        => 'nullable|exists:users,id',
            'section_id'    => 'nullable|exists:sections,id',
            'order_by'       => 'nullable', //[ 0=>mostRent , 1=>hightPrice , 2=>lowPrice, 3=> finalRate ]
        ]);


        /** Send Error Massages **/
        if ($validate->fails()) {
            return response()->json([
                'key'     => '0',
                'massage' => $validate->errors()->first()
            ]);
        }

        /** Get Data **/
        $results         = Service::query();
        //$results->where('section_id', $request->section_id);
        $results->whereIn('id', ArrayServicesSections($request->section_id));

        if ($request->order_by == '0')
            $results->orderBy('rent_count', 'desc');
        elseif ($request->order_by == '1')
            $results->orderBy('price', 'desc');
        elseif ($request->order_by == '2')
            $results->orderBy('price', 'asc');
        elseif ($request->order_by == '3')
            $results->orderBy('finalRate', 'desc');
        else
            $results->orderBy('id', 'desc');

        $all_services = $results->get();

        /** Send Data **/
        $data  = [];
        $i = 0;
        foreach ($all_services as $service) {
            //if(!is_null($service->Section) && $service->Section->active == 1){
            $data[$i] = showService($service, $this->lang, $request->user_id);
            $i++;
            //}
        }

        return response()->json([
            'key'     => '1',
            'massage' => trans('api.send'),
            'data'    => $data
        ]);
    }

    #show by search
    public function show_service_by_search(Request $request)
    {
        /** Validate Request **/
        $validate = Validator::make($request->all(), [
            'user_id'        => 'nullable|exists:users,id',
            'section_id'    => 'nullable|exists:sections,id',
            'title'           => 'nullable',
            'lat'           => 'nullable',
            'lng'           => 'nullable',
            'start_date'       => 'required_with:end_date',
            'end_date'       => 'required_with:start_date',
        ]);


        /** Send Error Massages **/
        if ($validate->fails()) {
            return response()->json([
                'key'     => '0',
                'massage' => $validate->errors()->first()
            ]);
        }


        /** Get Data **/
        $results         = Service::query();
        if (!is_null($request->lat) && !is_null($request->lng)) {
            $lat  = $request->lat;
            $lng  = $request->lng;
            $results->having('distance', '<=', '500')
                ->select(
                    DB::raw("*,
                (3959 * ACOS(COS(RADIANS($lat))
                * COS(RADIANS(lat))
                * COS(RADIANS($lng) - RADIANS(lng))
                + SIN(RADIANS($lat))
                * SIN(RADIANS(lat)))) AS distance")
                );
        }

        if (!is_null($request->section_id))
            //$results->where('section_id', $request->section_id);
            $results->whereIn('id', ArrayServicesSections($request->section_id));

        if (!is_null($request->title))
            $results->where('title_ar', 'like', '%' . $request->title . '%')->orWhere('title_en', 'like', '%' . $request->title . '%');

        $all_services = $results->get();

        /** Send Data **/
        $data  = [];
        $i = 0;
        //check date and quantity
        if ($request->start_date && $request->end_date) {
            foreach ($all_services as $service) {
                if (checkServiceDateQuantity($service->id, $request->start_date, $request->end_date)) {
                    //getQuantity($service->id,$request->start_date);
                    $data[$i] = showService($service, $this->lang, $request->user_id);
                    $i++;
                }
            }
        } else {
            foreach ($all_services as $service) {
                $data[$i] = showService($service, $this->lang, $request->user_id);
                $i++;
            }
        }

        return response()->json([
            'key'     => '1',
            'massage' => trans('api.send'),
            'data'    => $data
        ]);
    }

    #show by provider
    public function show_service_by_provider(Request $request)
    {
        /** Validate Request **/
        $validate = Validator::make($request->all(), [
            'provider_id'    => 'required|exists:users,id',
            'user_id'         => 'nullable|exists:users,id',
        ]);

        /** Send Error Massages **/
        if ($validate->fails()) {
            return response()->json([
                'key'     => '0',
                'massage' => $validate->errors()->first()
            ]);
        }

        /** Get Data **/
        $all_services = Service::where('user_id', $request->provider_id)->get();

        /** Send Data **/
        $data  = [];
        $i = 0;
        foreach ($all_services as $service) {
            //if(!is_null($service->Section) && $service->Section->active == 1){
            $data[$i] = showService($service, $this->lang, $request->user_id);
            $i++;
            //}
        }

        return response()->json([
            'key'     => '1',
            'massage' => trans('api.send'),
            'data'    => $data
        ]);
    }

    #show by favourite
    public function show_service_by_favourite(Request $request)
    {
        /** Validate Request **/
        $validate = Validator::make($request->all(), [
            'user_id'    => 'required|exists:users,id',
        ]);

        /** Send Error Massages **/
        if ($validate->fails()) {
            return response()->json([
                'key'     => '0',
                'massage' => $validate->errors()->first()
            ]);
        }

        /** Get Data **/
        $all_services = User::showFavourite($request->user_id);

        /** Send Data **/
        $data  = [];
        $i = 0;
        foreach ($all_services as $id) {
            $service = Service::find($id);
            //if(!is_null($service->Section) && $service->Section->active == 1){
            $data[$i] = showService($service, $this->lang, $request->user_id);
            $i++;
            //}
        }

        return response()->json([
            'key'     => '1',
            'massage' => trans('api.send'),
            'data'    => $data
        ]);
    }

    #store new favourite
    public function store_service_by_favourite(Request $request)
    {
        /** Validate Request **/
        $validate = Validator::make($request->all(), [
            'user_id'        => 'required|exists:users,id',
            'service_id'    => 'required|exists:services,id',
        ]);

        /** Send Error Massages **/
        if ($validate->fails()) {
            return response()->json([
                'key'     => '0',
                'massage' => $validate->errors()->first()
            ]);
        }

        /** Get Data **/
        $check = Favourite::where('user_id', $request->user_id)->where('service_id', $request->service_id)->first();
        if (isset($check))
            $check->delete();
        else {
            $add = new Favourite;
            $add->user_id = $request->user_id;
            $add->service_id = $request->service_id;
            $add->save();
        }

        return response()->json([
            'key'     => '1',
            'massage' => trans('api.save')
        ]);
    }

    #delete favourite
    public function delete_service_by_favourite(Request $request)
    {
        /** Validate Request **/
        $validate = Validator::make($request->all(), [
            'user_id'        => 'required|exists:users,id',
            'service_id'    => 'required|exists:services,id',
        ]);

        /** Send Error Massages **/
        if ($validate->fails()) {
            return response()->json([
                'key'     => '0',
                'massage' => $validate->errors()->first()
            ]);
        }

        /** Get Data **/
        Favourite::where('user_id', $request->user_id)->where('service_id', $request->service_id)->delete();

        return response()->json([
            'key'     => '1',
            'massage' => trans('api.delete')
        ]);
    }

    /*
    |----------------------------------------------------|
    |----------------------------------------------------|
    |                  order Page Start                  |
    |----------------------------------------------------|
    |----------------------------------------------------|
    */

    #check befor start store order and count price
    public function check_order_date(Request $request)
    {
        /** Validate Request **/
        $validate = Validator::make($request->all(), [
            'service_id'       => 'required|exists:services,id',
            'start_date'       => 'required',
            'end_date'       => 'required',
        ]);


        /** Send Error Massages **/
        if ($validate->fails()) {
            return response()->json([
                'key'     => '0',
                'massage' => $validate->errors()->first()
            ]);
        }

        //check date//
        #old date already taken
        $service = Service::find($request->service_id); //get service data
        $count   = $service->quantity; //get service quantity
        $orders  = Order::where('service_id', $request->service_id)->where('status', '<', '2')->get(); //get all orders used this servic
        $old_dates = []; // all token dates will push here
        foreach ($orders as $order) {
            $arr = createDateRangeArray($order->start_date, $order->end_date); //get all date range
            foreach ($arr as $value) {
                array_push($old_dates, $value); //push date in old _dates array
            }
        }
        #check with old date
        $dates = createDateRangeArray($request->start_date, $request->end_date); //get all date range
        foreach ($dates as $value) {
            if (in_array($value, $old_dates)) { //check if date in old_dates array # if this date is token by another order
                if ($count > 1) { //if order can take more than one order
                    $orderCount = getOrdersCount($request->service_id, $value); //count how many order reserve this service to compare with the service quantity
                    //if there is service at stock not using
                    if ($orderCount < $count)
                        continue;
                }
                //if all quantity was already token
                return response()->json([
                    'key'     => '0',
                    'massage' => $value . trans('api.takenDate')
                ]);
            }
        }

        #check with old date
        $dates      = createDateRangeArray($request->start_date, $request->end_date); //get all date range
        $reserve    = true;
        $online     = $service->payment_method == 0 ? false : true;
        $cashe      = $service->payment_method == 1 ? false : true;
        $not_reserve_date   = [];
        foreach ($dates as $date) {
            $check = Service_date::where('service_id', $service->id)->whereDate('date', $date)->get();
            foreach ($check as $item) {
                if ($item->type == 1) {
                    $reserve = false;
                    array_push($not_reserve_date, $date);
                }
            }
        }



        //count total price//
        #specific price
        $offers = Service_price::where('service_id', $request->service_id)->get();
        $all_prices = [];
        foreach ($offers as $offer) {
            $arr = createDateRangeArray($offer->start_date, $offer->end_date);
            foreach ($arr as $value) {
                $all_prices[$value] = $offer->price;
            }
        }
        //count total price
        $total = 0;
        $price = Service::find($request->service_id)->price;
        $dates = createDateRangeArray($request->start_date, $request->end_date);
        foreach ($dates as $value) {
            if (isset($all_prices[$value]))
                $total += $all_prices[$value];
            else
                $total += $price;
        }

        $data = [
            'total'     => $total,
            'reserve'   => $reserve,
            'online'    => $online,
            'cashe'     => $cashe,
            'not_reserve_date' => $not_reserve_date,
        ];

        return response()->json([
            'key'     => '1',
            'massage' => 'مسموح',
            'data'      => $data
        ]);
    }

    #store order
    public function store_order(Request $request)
    {
        /** Validate Request **/
        $validate = Validator::make($request->all(), [
            'user_id'        => 'required|exists:users,id',
            'service_id'     => 'required|exists:services,id',
            'start_date'     => 'required',
            'end_date'       => 'required',
            'price'          => 'required',
        ]);


        /** Send Error Massages **/
        if ($validate->fails()) {
            return response()->json([
                'key'     => '0',
                'massage' => $validate->errors()->first()
            ]);
        }

        /* data */
        $service = Service::find($request->service_id);

        $provider = User::whereId($service->user_id)->first();
        if ($provider->stop_reserving == 1) {
            return response()->json([
                'key'     => '0',
                'massage' => trans('api.reservationStop')
            ]);
        }


        /** store Data **/
        $order = new Order;
        $order->price              = $request->price;
        $order->start_date         = $request->start_date;
        $order->end_date           = $request->end_date;
        $order->user_id            = $request->user_id;
        $order->provider_id        = $service->user_id;
        $order->service_id         = $service->id;
        $order->payment_method     = $service->payment_method;
        // 0=new , 1=agree , 2=refused , 3=deletedByClient , 4=deletedByProvider , 5=finish, 6 = delayed by provider
        $order->status             = '1'; // changed by owner always agree
        $order->save();

        // increase sales for filter
        $service->rent_count += 1;
        $service->save();


        $user = User::find($order->provider_id);
        $profit = (float) settings('profit_percent');
        $price  = (float) $request->price;
        $debt   = $price * $profit / 100;

        // $user->total_profit = $user->total_profit + $debt;
        // $user->save();


        /** send notification to provider **/
        sendNotify($order->user_id, $order->provider_id, $order->id);

        #Send FCM To provider
        if (isset($user) && !is_null($user->device_id)) {
            $device_id          = $user->device_id;
            $device_type        = $user->device_type;
            $lang = $user->lang;
            $data = [
                'title_ar'    => 'طلب',
                'title_en'    => 'order',
                'active_ar'   => 'لديك طلب جديد برقم : ' . $order->id,
                'active_en'   => 'you have new order no: ' . $order->id,
            ];
            $all_data   = [];
            $all_data['title']      = $data['title_' . $lang];
            $all_data['msg']        = $data['active_' . $lang];
            $all_data['status']     = 0; //new order

            Send_FCM_Badge($device_id, $all_data, $device_type);
        }

        /** send Data **/
        $data  = [];
        if (!is_null($order->User) && !is_null($order->Provider) && !is_null($order->Service))
            $data = showOrder($order, $this->lang);

        return response()->json([
            'key'     => '1',
            'massage' => trans('api.save'),
            'data'    => $data
        ]);
    }

    #all agree user order
    public function show_all_user_order(Request $request)
    {
        /** Validate Request **/
        $validate = Validator::make($request->all(), [
            'user_id'   => 'required|exists:users,id',
        ]);

        /** Send Error Massages **/
        if ($validate->fails()) {
            return response()->json([
                'key'       => '0',
                'massage'   => $validate->errors()->first()
            ]);
        }

        /** Send Data **/
        $data  = [];
        $i = 0;
        foreach (Order::User_orders($request->user_id) as $order) {
            if (!is_null($order->User) && !is_null($order->Provider) && !is_null($order->Service)) {
                $data[$i] = showOrder($order, $this->lang);
                $i++;
            }
        }

        return response()->json([
            'key'     => '1',
            'massage' => trans('api.send'),
            'data'    => $data
        ]);
    }

    #all agree provider order
    public function show_all_provider_order(Request $request)
    {
        /** Validate Request **/
        $validate = Validator::make($request->all(), [
            'user_id'   => 'required|exists:users,id',
        ]);

        /** Send Error Massages **/
        if ($validate->fails()) {
            return response()->json([
                'key'       => '0',
                'massage'   => $validate->errors()->first()
            ]);
        }

        /** Send Data **/
        $data  = [];
        $i = 0;
        foreach (Order::Provider_orders($request->user_id) as $i => $order) {
            if (!is_null($order->User) && !is_null($order->Provider) && !is_null($order->Service)) {
                $data[$i] = showOrder($order, $this->lang);
                $i++;
            }
        }

        return response()->json([
            'key'     => '1',
            'massage' => trans('api.send'),
            'data'    => $data
        ]);
    }

    #all agree provider order
    public function search_provider_order(Request $request)
    {
        /** Validate Request **/
        $validate = Validator::make($request->all(), [
            'user_id'   => 'required|exists:users,id',
            'date'      => 'required',
        ]);

        /** Send Error Massages **/
        if ($validate->fails()) {
            return response()->json([
                'key'       => '0',
                'massage'   => $validate->errors()->first()
            ]);
        }

        /** Send Data **/
        $data  = [];
        $i = 0;
        foreach (Order::Provider_orders_by_date($request->user_id, $request->date) as $i => $order) {
            if (!is_null($order->User) && !is_null($order->Provider) && !is_null($order->Service)) {
                $data[$i] = showOrder($order, $this->lang);
                $i++;
            }
        }

        return response()->json([
            'key'     => '1',
            'massage' => trans('api.send'),
            'data'    => $data
        ]);
    }

    #show_order
    public function show_order(Request $request)
    {
        /** Validate Request **/
        $validate = Validator::make($request->all(), [
            'order_id'   => 'required|exists:orders,id',
        ]);

        /** Send Error Massages **/
        if ($validate->fails()) {
            return response()->json([
                'key'       => '0',
                'massage'   => $validate->errors()->first()
            ]);
        }

        /** Send Data **/
        $order = Order::find($request->order_id);

        $data  = [];
        if (!is_null($order->User) && !is_null($order->Provider) && !is_null($order->Service))
            $data = showOrder($order, $this->lang);

        return response()->json([
            'key'     => '1',
            'massage' => trans('api.send'),
            'data'    => $data
        ]);
    }

    #user delete_order
    public function user_delete_order(Request $request)
    {
        /** Validate Request **/
        $validate = Validator::make($request->all(), [
            'order_id'   => 'required|exists:orders,id',
        ]);

        /** Send Error Massages **/
        if ($validate->fails()) {
            return response()->json([
                'key'       => '0',
                'massage'   => $validate->errors()->first()
            ]);
        }

        /** Send Data **/
        $order = Order::find($request->order_id);
        $order->user_seen = 0;
        if ($order->status < 2) {
            $order->provider_seen = '0';
            $order->status = '3'; //deleted by client

            #Send FCM To Provider
            $user = User::find($order->provider_id);
            if (isset($user) && !is_null($user->device_id)) {
                $device_id          = $user->device_id;
                $device_type        = $user->device_type;
                $lang = $user->lang;
                $data = [
                    'title_ar'    => 'الطلبات',
                    'title_en'    => 'orders',
                    'active_ar'   => 'تم الغاء الطلب برقم : ' . $order->id,
                    'active_en'   => 'order no: ' . $order->id . 'is canceled',
                ];
                $all_data   = [];
                $all_data['title']      = $data['title_' . $lang];
                $all_data['msg']        = $data['active_' . $lang];
                $all_data['status']     = 3; //canceled order

                Send_FCM_Badge($device_id, $all_data, $device_type);
            }
        }
        $order->save();

        return response()->json([
            'key'     => '1',
            'massage' => trans('api.delete')
        ]);
    }

    #provider delete_order
    public function provider_delete_order(Request $request)
    {
        /** Validate Request **/
        $validate = Validator::make($request->all(), [
            'order_id'   => 'required|exists:orders,id',
        ]);

        /** Send Error Massages **/
        if ($validate->fails()) {
            return response()->json([
                'key'       => '0',
                'massage'   => $validate->errors()->first()
            ]);
        }

        /** Send Data **/
        $order = Order::find($request->order_id);
        $order->provider_seen = 0;
        if ($order->status < 2) {
            $order->user_seen = '0';
            $order->status = '4'; //deleted by provider

            #Send FCM To Client
            $user = User::find($order->user_id);
            if (isset($user) && !is_null($user->device_id)) {
                $device_id          = $user->device_id;
                $device_type        = $user->device_type;
                $lang = $user->lang;
                $data = [
                    'title_ar'    => 'الطلبات',
                    'title_en'    => 'orders',
                    'active_ar'   => 'تم الغاء الطلب برقم : ' . $order->id,
                    'active_en'   => 'order no: ' . $order->id . 'is canceled',
                ];
                $all_data   = [];
                $all_data['title']      = $data['title_' . $lang];
                $all_data['msg']        = $data['active_' . $lang];
                $all_data['status']     = 3; //canceled order

                Send_FCM_Badge($device_id, $all_data, $device_type);
            }
        }
        $order->save();

        return response()->json([
            'key'     => '1',
            'massage' => trans('api.delete')
        ]);
    }

    # agree or refused order or
    public function change_order_status(Request $request)
    {
        /** Validate Request **/
        $validate = Validator::make($request->all(), [
            'order_id'   => 'required|exists:orders,id',
            'status'     => 'required' // 1=agree , 2=refuesd
        ]);

        /** Send Error Massages **/
        if ($validate->fails()) {
            return response()->json([
                'key'       => '0',
                'massage'   => $validate->errors()->first()
            ]);
        }

        /** Send Data **/
        $order = Order::find($request->order_id);
        $order->status = $request->status == '1' ? '1' : '2';
        $order->save();

        #Send FCM To Client
        if ($order->status == '2') {
            /** Send FCM **/
            $user = User::find($order->user_id);
            if (isset($user) && !is_null($user->device_id)) {
                $device_id          = $user->device_id;
                $device_type        = $user->device_type;
                $lang = $user->lang;
                $data = [
                    'title_ar'    => 'الطلبات',
                    'title_en'    => 'orders',
                    'active_ar'   => 'تم رفض الطلب برقم : ' . $order->id,
                    'active_en'   => 'order no: ' . $order->id . 'is refused',
                ];
                $all_data   = [];
                $all_data['title']      = $data['title_' . $lang];
                $all_data['msg']        = $data['active_' . $lang];
                $all_data['status']     = 2; //refused order

                Send_FCM_Badge($device_id, $all_data, $device_type);
            }
        } else {
            /** Send FCM **/
            $user = User::find($order->user_id);
            if (isset($user) && !is_null($user->device_id)) {
                $device_id          = $user->device_id;
                $device_type        = $user->device_type;
                $lang = $user->lang;
                $data = [
                    'title_ar'    => 'الطلبات',
                    'title_en'    => 'orders',
                    'active_ar'   => 'تم قبول الطلب برقم : ' . $order->id,
                    'active_en'   => 'order no: ' . $order->id . 'is agreed',
                ];
                $all_data   = [];
                $all_data['title']      = $data['title_' . $lang];
                $all_data['msg']        = $data['active_' . $lang];
                $all_data['status']     = 1; //agreed order

                Send_FCM_Badge($device_id, $all_data, $device_type);
            }
        }

        return response()->json([
            'key'     => '1',
            'massage' => $request->status == '1' ? trans('api.agreeOrder') : trans('api.refusedOrder')
        ]);
    }

    # delay order --- always accepted -> site owner --- status = 6
    public function DelayAcceptedOrder(Request $request)
    {
        /** Validate Request **/
        $validate = Validator::make($request->all(), [
            'order_id'   => 'required|exists:orders,id',
            'date'       => 'required'
        ]);

        /** Send Error Massages **/
        if ($validate->fails()) {
            return response()->json([
                'key'       => '0',
                'massage'   => $validate->errors()->first()
            ]);
        }

        /** Send Data **/
        $order = Order::find($request->order_id);
        $day_count     = Carbon::parse($order->start_date)->diffInDays(Carbon::parse($order->end_date));
        //$order->status == 1 ?  $order->status = '6' : '';
        $order->start_date = Carbon::parse($request->date)->format('Y-m-d');
        $order->end_date   = Carbon::parse($request->date)->addDays($day_count)->format('Y-m-d');
        $order->delay_date = Carbon::parse($request->date)->format('Y-m-d');
        $order->save();

        #Send FCM To Client
        if ($order->status == '6') {
            /** Send FCM **/
            $user = User::find($order->user_id);
            if (isset($user) && !is_null($user->device_id)) {
                $device_id          = $user->device_id;
                $device_type        = $user->device_type;
                $lang = $user->lang;
                $data = [
                    'title_ar'    => 'الطلبات',
                    'title_en'    => 'orders',
                    'active_ar'   => 'تم تاجيل الطلب برقم : ' . $order->id,
                    'active_en'   => 'order no: ' . $order->id . 'is delayed',
                ];
                $all_data   = [];
                $all_data['title']      = $data['title_' . $lang];
                $all_data['msg']        = $data['active_' . $lang];
                $all_data['status']     = 6; //delay order

                Send_FCM_Badge($device_id, $all_data, $device_type);
            }
        }

        return response()->json([
            'key'     => '1',
            'massage' => trans('api.orderDelayed')
        ]);
    }

    # show delayed orders for user or provider
    public function show_delayed_orders(Request $request)
    {

        /** Validate Request **/
        $validate = Validator::make($request->all(), [
            'user_id'   => 'required|exists:users,id',
        ]);

        /** Send Error Massages **/
        if ($validate->fails()) {
            return response()->json([
                'key'       => '0',
                'massage'   => $validate->errors()->first()
            ]);
        }

        /** Send Data **/
        $user = User::whereId($request->user_id)->first();
        if ($user->provider == 1) {
            $delayedOrders = Order::where('user_seen', '1')->where('provider_id', $user->id)->where('status', '6')->get();
        } else {
            $delayedOrders = Order::where('user_seen', '1')->where('user_id', $user->id)->where('status', '6')->get();
        }
        $data  = [];
        $i = 0;
        foreach ($delayedOrders as $order) {
            if (!is_null($order->User) && !is_null($order->Provider) && !is_null($order->Service)) {
                $data[$i] = showOrder($order, $this->lang);
                $i++;
            }
        }

        return response()->json([
            'key'     => '1',
            'massage' => trans('api.send'),
            'data'    => $data
        ]);
    }

    /*
    |----------------------------------------------------|
    |----------------------------------------------------|
    |             notification Page Start                |
    |----------------------------------------------------|
    |----------------------------------------------------|
    */

    #all agree provider notification
    public function show_all_provider_notification(Request $request)
    {
        /** Validate Request **/
        $validate = Validator::make($request->all(), [
            'user_id'   => 'required|exists:users,id',
        ]);

        /** Send Error Massages **/
        if ($validate->fails()) {
            return response()->json([
                'key'       => '0',
                'massage'   => $validate->errors()->first()
            ]);
        }

        /** Send Data **/
        $data  = [];
        $message = 'message_' . $this->lang;
        foreach (Notification::getUserNotification($request->user_id) as $i => $notify) {
            $data[$i]['id']                 = $notify->id;
            $data[$i]['message']             = is_null($notify->$message) ? '' : $notify->$message;
            $data[$i]['date']                 = is_null($notify->created_at) ? '' : date('Y-m-d', strtotime($notify->created_at));
            // $data[$i]['seen'] 			= $notify->seen == '1' ? '1' : '0';
            $data[$i]['order_id']             = is_null($notify->Order) ? 0 : $notify->Order->id;
            $data[$i]['order_status']         = is_null($notify->Order) || is_null($notify->Order->status) ? 100 : $notify->Order->status;
            $data[$i]['order_status_msg']     = is_null($notify->Order) || is_null($notify->Order->status) ? showStatusMsg('0') : showStatusMsg($notify->Order->status);
            $data[$i]['user_id']             = is_null($notify->From) ? 0 : $notify->From->id;
            $data[$i]['user_name']             = is_null($notify->From) || is_null($notify->From->name) ? '' : $notify->From->name;
            $data[$i]['user_phone']         = is_null($notify->From) || is_null($notify->From->phone) ? '' : $notify->From->phone;
            $data[$i]['user_avatar']         = is_null($notify->From) || is_null($notify->From->avatar) ? url('images/users/default.png') : url('public/images/users/' . $notify->From->avatar);
        }

        return response()->json([
            'key'     => '1',
            'massage' => trans('api.send'),
            'data'    => $data
        ]);
    }

    #provider delete notification
    public function provider_delete_notification(Request $request)
    {
        /** Validate Request **/
        $validate = Validator::make($request->all(), [
            'notification_id'   => 'required|exists:notifications,id',
        ]);

        /** Send Error Massages **/
        if ($validate->fails()) {
            return response()->json([
                'key'       => '0',
                'massage'   => $validate->errors()->first()
            ]);
        }

        /** Send Data **/
        $notify = Notification::find($request->notification_id);
        $notify->delete();

        return response()->json([
            'key'     => '1',
            'massage' => trans('api.delete')
        ]);
    }

    /*
    |----------------------------------------------------|
    |----------------------------------------------------|
    |             Rate and comment Start                |
    |----------------------------------------------------|
    |----------------------------------------------------|
    */

    # provider delete notification - if old will overwrite
    public function RateService(Request $request)
    {
        /** Validate Request **/
        $validate = Validator::make($request->all(), [
            'user_id'           => 'required|exists:users,id',
            'provider_id'       => 'required|exists:services,id',
            'rate'              => 'required|numeric|in:.5,1,1.5,2,2.5,3,3.5,4,4.5,5',
            'comment'           => 'nullable|max:200',
        ]);

        /** Send Error Massages **/
        if ($validate->fails()) {
            return response()->json([
                'key'       => '0',
                'massage'   => $validate->errors()->first()
            ]);
        }

        $edit = Rate::where('user_id', $request->user_id)->where('service_id', $request->provider_id)->first();
        if ($edit) {

            $edit->rate              = $request->rate;
            $edit->service_id     = $request->provider_id;
            $edit->comment           = $request->comment != null ? $request->comment : "";
            $edit->user_id           = $request->user_id != null ? $request->user_id : 0;
            $edit->save();
        } else {

            $new = new Rate;
            $new->rate              = $request->rate;
            $new->service_id        = $request->provider_id;
            $new->comment           = $request->comment != null ? $request->comment : "";
            $new->user_id           = $request->user_id;
            $new->save();
        }

        updateRates();

        $data = [];
        $serviceRates = Rate::where('service_id', $request->provider_id)->orderBy('id', 'desc')->get();
        foreach ($serviceRates as $rate) {
            $data[] = [
                'id'            => $rate->id,
                'rate'          => $rate->rate,
                'comment'       => $rate->comment,
                'user_id'       => $rate->User->id,
                'user_name'     => $rate->User->name,
                'created_at'    => date('Y-m-d', strtotime($rate->created_at)),
            ];
        }

        return response()->json([
            'key'     => '1',
            'massage' => "success",
            'date' => $data
        ]);
    }

    # provider delete comment only not rate
    public function DeleteComment(Request $request)
    {
        /** Validate Request **/
        $validate = Validator::make($request->all(), [
            'rate_id'           => 'required|exists:rates,id',
        ]);

        /** Send Error Massages **/
        if ($validate->fails()) {
            return response()->json([
                'key'       => '0',
                'massage'   => $validate->errors()->first()
            ]);
        }

        $rate = Rate::whereId($request->rate_id)->first();
        $rate->comment = "";
        $rate->save();

        return response()->json([
            'key'     => '1',
            'massage' => trans('api.delete')
        ]);
    }

    #close dates
    public function Close_Dates(Request $request)
    {

        $validator = Validator::make($request->all(), [
            'user_id'       => 'required|exists:users,id',
            'dates'         => 'required'
        ]);

        if ($validator->passes()) {

            $Services = Service::where('user_id', $request['user_id'])->get();
            if (!empty($request['dates'])) {
                foreach ($Services as $Service) {
                    foreach (json_decode($request['dates']) as $Date) {
                        $Close                = new Service_date();
                        $Close['date']        = Carbon::parse($Date);
                        $Close['service_id']  = $Service['id'];
                        $Close['type']        = 1;
                        $Close->Save();
                    }
                }
            }

            return response()->json(['key' => '1', 'value' => '1', 'msg' => 'Done']);
        } else {
            foreach ((array) $validator->errors() as $key => $value) {
                foreach ($value as $msg) {
                    return response()->json(['key' => '0', 'value' => '0', 'msg' => $msg[0]]);
                }
            }
        }
    }

    #update_section_payment_method
    public function update_section_payment_method(Request $request)
    {

        $validator = Validator::make($request->all(), [
            'user_id'       => 'required|exists:users,id',
            'section_id'    => 'required|exists:sections,id',
            'type'          => 'required' //0=>cash , 1=>Visa , 2=Both of them
        ]);

        /** Send Error Massages **/
        if ($validator->fails()) {
            return response()->json([
                'key'       => '0',
                'massage'   => $validator->errors()->first()
            ]);
        }

        Service::where('user_id', $request['user_id'])->where('section_id', $request['section_id'])->update([
            'payment_method' => $request->type
        ]);

        return response()->json([
            'key'     => '1',
            'massage' => trans('api.save')
        ]);
    }

    #stop order
    public function stop_order(Request $request)
    {

        $validator = Validator::make($request->all(), [
            'user_id'       => 'required|exists:users,id',
        ]);

        /** Send Error Massages **/
        if ($validator->fails()) {
            return response()->json([
                'key'       => '0',
                'massage'   => $validator->errors()->first()
            ]);
        }

        $user = User::whereId($request->user_id)->first();
        $user->stop_service = !$user->stop_service;
        $user->save();

        $Services = Service::where('user_id', $request['user_id'])->get();
        foreach ($Services as $Service) {
            $Service->close = !$Service->close;
            $Service->Save();
        }

        return response()->json([
            'key'     => '1',
            'massage' => trans('api.save')
        ]);
    }
}
