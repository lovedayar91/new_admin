<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Awesome Language Package (AWT)
    |--------------------------------------------------------------------------
    |
    | The following language lines are used during processing for various
    | messages that we need to display to the user. You are free to modify
    | these language lines according to your application's requirements.
    | but you have to know that`s those lines are dynamically created by out package
    |
    */


    "الرئيسية" => "Home",
    "الرسائل" => "contact",
    "بحث" => "Search",
    "مشاهدة الكل" => "view all",
"الأعضاء"=>"Member",
"المندوبين"=>"Delegates",
"الأقسام"=>"Sections",
"الخدمات"=>"Services",
"الدول"=>"States",
"المدن"=>"Cities",
"الاحياء"=>"Dreams",
"تواصل معنا"=>"Contact us",
"أحدث الاعضاء"=>"The latest enemies",
"أعضاء جدد"=>"New members",
"عرض الكل"=>"View all",
"أحدث المندوبين"=>"Latest delegates",
"مندوبين جدد"=>"Delegates",
"عرض الكل"=>"View all",
"احدث الخدمات"=>"One of the services",
"عرض الكل"=>"View all",
"الإعدادات"=>"Settings",
"الصلاحيات"=>"Powers",
"المديرين"=>"Managers",
"الأعضاء"=>"Member",
"المندوبين"=>"Delegates",
"الصفحات الاساسية"=>"Pages basic",
"الأقسام"=>"Sections",
"الخدمات"=>"Services",
"الدول"=>"States",
"المدن"=>"Cities",
"الأحياء"=>"Biology",
"تواصل معنا"=>"Contact us",
"تقارير لوحة التحكم"=>"Reports Control Panel",
"اضافة"=>"Extra",
"إرسال للكل"=>"Send the word",
"حذف المحدد"=>"Delete selected",
"حذف الكل"=>"Delete all",
"الصورة"=>"The picture",
"الأسم"=>"Name",
"الجوال"=>"Mobile",
"البريد الإلكتروني"=>"E-mail",
"العنوان"=>"Title",
"حالة العضو"=>"The case of the member",
"وقت التسجيل"=>"Delivery time",
"التحكم"=>"Check",
"إرسال أشعار"=>"Send poems",
"الرسالة"=>"Message",
"اكتب رسالتك ..."=>"Type your message ...",
"إرسال"=>"Send",
"إضافة عضو"=>"To add a member",
"الصورة الشخصية"=>"Profile picture",
"الأسم"=>"Name",
"البريد الإلكتروني"=>"E-mail",
"الجوال"=>"Mobile",
"العنوان"=>"Title",
"كلمة المرور"=>"Password",
"حفظ"=>"Save",
"إغلاق"=>"Close",
"تعديل عضو"=>"Adjustment member",
"الصورة الشخصية"=>"Profile picture",
"الأسم"=>"Name",
"البريد الإلكتروني"=>"E-mail",
"الجوال"=>"Mobile",
"العنوان"=>"Title",
"حفظ"=>"Save",
"إغلاق"=>"Close",
"تأكيد الحذف"=>"Confirm the deletion",
"هل تريد الاستمرار في عملية الحذف ؟"=>"Do you want to continue the deletion process is?",
"تأكيد"=>"Confirmation",
"إلغاء"=>"Cancel",
"تأكيد الحذف"=>"Confirm the deletion",
"هل تريد الاستمرار في عملية الحذف ؟"=>"Do you want to continue the deletion process is?",
"تأكيد"=>"Confirmation",
"إلغاء"=>"Cancel",
"حالة المندوب"=>"The case of the delegate",
"إضافة مندوب"=>"Add rep",
"تعديل مندوب"=>"Modify delegate",
"إضافة قسم"=>"Add section",
"العنوان بالعربية"=>"Title in Arabic",
"العنوان بالإنجليزية"=>"The title in English",
"التفاصيل القصيرة بالعربية"=>"Details of short Arabic",
"التفاصيل القصيرة بالإنجليزية"=>"Details of short English",
"التفاصيل الكاملة بالعربية"=>"Full details of Arabic",
"التفاصيل الكاملة بالإنجليزية"=>"Full details in English",
"تعديل قسم"=>"Amend Section",
"العنوان بالعربية"=>"Title in Arabic",
"العنوان بالإنجليزية"=>"The title in English",
"التفاصيل القصيرة بالعربية"=>"Details of short Arabic",
"التفاصيل القصيرة بالإنجليزية"=>"Details of short English",
"التفاصيل الكاملة بالعربية"=>"Full details of Arabic",
"التفاصيل الكاملة بالإنجليزية"=>"Full details in English",
"القسم"=>"Section",
"إضافة خدمة"=>"Add service",
"القسم"=>"Section",
"تعديل خدمة"=>"Modify the service",
"القسم"=>"Section",
"إضافة دولة"=>"Add state",
"كود الدولة"=>"Country code",
"رمز العملة"=>"The currency symbol",
"تعديل دولة"=>"Modify the state of",
"كود الدولة"=>"Country code",
"رمز العملة"=>"The currency symbol",
"إضافة مدينة"=>"Add a city",
"الدولة"=>"The state",
"تعديل مدينة"=>"To amend city of",
"الدولة"=>"The state",
"إضافة حي"=>"Add live",
"المدينة"=>"The city",
"تعديل حي"=>"Modified live",
"المدينة"=>"The city",
"عرض"=>"View",
"حذف"=>"Delete",
"التفاصيل"=>"Details",
"تسجيل الدخول"=>"Login",
"تسجيل الدخول"=>"Login",
"دخول"=>"Entry",
"تم تسجيل الدخول بنجاح"=>"You have successfully logged in",
"البيانات غير صحيحة قم بإعادة المحاولة"=>"Incorrect data re portable",
    #AWTLINEHELPER
    #don`t Remove the up line at all

];
